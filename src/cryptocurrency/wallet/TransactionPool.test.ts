import { Transaction } from './Transaction'
import { TransactionPool } from './TransactionPool'
import { Wallet } from './Wallet'

describe('TransactionPool', () => {
    let tp
    let wallet
    let transaction

    beforeEach(() => {
        tp = new TransactionPool()
        wallet = new Wallet()
        transaction = Transaction.newTransaction(wallet, 'r4nd-4dr355', 30)
        tp.updateOrAddTransaction(transaction)
    })

    it('should add a transaction to the pool', () => {
        expect(tp.transactions.find(t => t.id === transaction.id)).toEqual(
            transaction
        )
    })

    it('should update a transaction in the pool', () => {
        const oldTransaction = JSON.stringify(transaction)
        const newTransaction = transaction.update(wallet, 'foo-4ddr355', 40)
        tp.updateOrAddTransaction(newTransaction)
        expect(
            JSON.stringify(
                tp.transactions.find(t => t.id === newTransaction.id)
            )
        ).not.toEqual(oldTransaction)
    })
})
