import { Transaction } from './Transaction'

export class TransactionPool {
    private transactions: Transaction[] = []

    public updateOrAddTransaction(transaction: Transaction) {
        let transactionWithId = this.transactions.find(
            t => t.id === transaction.id
        )

        if (transactionWithId) {
            this.transactions[
                this.transactions.indexOf(transactionWithId)
            ] = transaction
        } else {
            this.transactions.push(transaction)
        }
    }

    public existingTransaction(address: string): Transaction {
        return this.transactions.find(t => t.input.address === address)
    }

    public getTransactions(): Transaction[] {
        return this.transactions
    }
}
