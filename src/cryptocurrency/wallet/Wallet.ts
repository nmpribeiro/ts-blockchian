import { INITIAL_BALANCE } from '../../config/config'
import { ec as EC } from 'elliptic'
import { ChainUtil } from '../../blockchain/ChainUtil'
import { TransactionPool } from './TransactionPool'
import { Transaction } from './Transaction'

export type ISignature = EC.Signature

export class Wallet {
    public balance: number
    public keyPair: EC.KeyPair
    public publicKey: string

    constructor() {
        this.balance = INITIAL_BALANCE
        this.keyPair = ChainUtil.genKeyPair()
        this.publicKey = this.keyPair.getPublic().encode('hex', false) as string
    }

    public toString(): string {
        return `Block:
    Balance   : ${this.balance}
    Public Key: ${this.publicKey.toString()}
    `
    }

    public sign(dataHash: string): EC.Signature {
        return this.keyPair.sign(dataHash)
    }

    public createTransaction(
        recipient: string,
        amount: number,
        transactionPool: TransactionPool
    ): Transaction {
        if (amount > this.balance) {
            console.log(
                `Amount: ${amount} exceeds the current balance: ${this.balance}`
            )
            return
        }

        let transaction = transactionPool.existingTransaction(this.publicKey)

        if (transaction) {
            transaction.update(this, recipient, amount)
        } else {
            transaction = Transaction.newTransaction(this, recipient, amount)
            transactionPool.updateOrAddTransaction(transaction)
        }
        return transaction
    }
}
