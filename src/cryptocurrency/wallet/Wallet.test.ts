import { Wallet } from './Wallet'
import { TransactionPool } from './TransactionPool'

describe('Wallet', () => {
    let wallet
    let tp

    beforeEach(() => {
        wallet = new Wallet()
        tp = new TransactionPool()
    })

    describe('creating a transaction', () => {
        let transaction
        let sendAmount
        let recipient

        beforeEach(() => {
            sendAmount = 50
            recipient = 'r4nd-4ddr355'
            transaction = wallet.createTransaction(recipient, sendAmount, tp)
        })

        describe('and doing the same transaction', () => {
            beforeEach(() => {
                wallet.createTransaction(recipient, sendAmount, tp)
            })

            it("should double the 'sendAmount' subtracted from the wallet balance", () => {
                expect(
                    transaction.outputs.find(
                        output => output.address === wallet.publicKey
                    ).amount
                ).toEqual(wallet.balance - sendAmount * 2)
            })

            it("should clone the 'sendAmount' output for the recipient", () => {
                expect(
                    transaction.outputs
                        .filter(output => output.address === recipient)
                        .map(output => output.amount)
                ).toEqual([sendAmount, sendAmount])
            })
        })
    })
})
