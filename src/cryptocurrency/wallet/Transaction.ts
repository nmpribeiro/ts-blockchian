import { ChainUtil } from '../../blockchain/ChainUtil'
import { ISignature, Wallet } from './Wallet'

interface ITransactionOutput {
    amount: number
    address: string
}

interface ITransactionInput extends ITransactionOutput {
    timestamp: number
    signature: ISignature
}

export class Transaction {
    public id: string
    public input: ITransactionInput
    public outputs: ITransactionOutput[]

    constructor() {
        this.id = ChainUtil.id()
        this.input = null
        this.outputs = []
    }

    public update(
        senderWallet: Wallet,
        recipient: string,
        amount: number
    ): Transaction {
        const senderOutput = this.outputs.find(
            output => output.address === senderWallet.publicKey
        )

        if (amount > senderOutput.amount) {
            console.log(`Amount ${amount} exceeds balance`)
            return
        }

        senderOutput.amount = senderOutput.amount - amount
        this.outputs.push({ amount, address: recipient })
        Transaction.signTransaction(this, senderWallet)

        return this
    }

    public static newTransaction(
        senderWallet: Wallet,
        recipient: string,
        amount: number
    ): Transaction {
        const transaction = new this()

        if (amount > senderWallet.balance) {
            console.log(`Amount ${amount} exceeds balance.`)
            return
        }

        transaction.outputs.push(
            ...[
                {
                    amount: senderWallet.balance - amount,
                    address: senderWallet.publicKey,
                },
                { amount, address: recipient },
            ]
        )
        Transaction.signTransaction(transaction, senderWallet)

        return transaction
    }

    public static signTransaction(
        transaction: Transaction,
        senderWallet: Wallet
    ) {
        transaction.input = {
            timestamp: Date.now(),
            amount: senderWallet.balance,
            address: senderWallet.publicKey,
            signature: senderWallet.sign(ChainUtil.hash(transaction.outputs)),
        }
    }

    public static verifyTransaction(transaction: Transaction) {
        return ChainUtil.verifySignature(
            transaction.input.address,
            transaction.input.signature,
            ChainUtil.hash(transaction.outputs)
        )
    }
}
