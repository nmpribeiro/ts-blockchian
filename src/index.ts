import { BlockChainApp } from './app'
import { Wallet } from './cryptocurrency/wallet/Wallet'

const wallet = new Wallet()
console.log(wallet.toString())

const blockChain = new BlockChainApp()
blockChain.initializeAppControllers()

if (require.main === module) {
    // true if file is executed
    // we might need to delay execution
    const delay = Number(process.env.DELAY) || 0
    console.log(`   Delay: ${delay}`)
    setTimeout(() => {
        console.log(`   LISTENING STARTED`)
        blockChain.listen()
    }, delay)
}

export default blockChain
