import Block from './Block'

export default class BlockChain {
    constructor(private _chain: Block[] = [Block.genesis()]) {}

    public addBlock(data: any): Block {
        const block = Block.mineBlock(this._chain[this._chain.length - 1], data)
        this._chain.push(block)

        return block
    }

    public isValidChain(chain: Block[]): boolean {
        if (JSON.stringify(chain[0]) !== JSON.stringify(Block.genesis())) {
            return false
        }

        for (let i = 1; i < chain.length; i++) {
            const block: Block = chain[i]
            const lastBlock = chain[i - 1]

            if (
                block.lastHash !== lastBlock.hash ||
                block.hash !== Block.blockHash(block)
            ) {
                return false
            }
        }

        return true
    }

    public replaceChain(newChain: Block[]): void {
        if (newChain.length <= this._chain.length) {
            console.log(
                'The received chain is not longer than the current chain'
            )
            return
        } else if (!this.isValidChain(newChain)) {
            console.log('The received chain is not valid')
            return
        }

        console.log('Replacing blockchain with the new chain')
        this._chain = newChain
    }

    public get chain(): Block[] {
        return this._chain
    }
}
