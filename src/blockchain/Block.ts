import { DIFFICULTY, MINE_RATE } from '../config/config'
import { ChainUtil } from './ChainUtil'

type DataType = any

export default class Block {
    // Genesis time
    private static GENESIS_TIME: number = 1565332728031

    public timestamp: number
    public lastHash: string
    public hash: string
    public data: DataType
    public nonce: number
    public difficulty: number

    constructor(
        timestamp: number,
        lastHash: string,
        hash: string,
        data: DataType,
        nonce: number,
        difficulty: number = DIFFICULTY
    ) {
        this.timestamp = timestamp
        this.lastHash = lastHash
        this.hash = hash
        this.data = data
        this.nonce = nonce
        this.difficulty = difficulty
    }

    public toString(): string {
        return `Block:
    TimeStamp : ${this.timestamp}
    Last Hash : ${this.lastHash.substring(0, 10)}
    Hash      : ${this.hash.substring(0, 10)}
    Nonce     : ${this.nonce}
    Difficulty: ${this.difficulty}
    Data      : ${this.data}`
    }

    public static genesis(): Block {
        return new this(Block.GENESIS_TIME, '-----', 'f1r57-h45h', [], 0)
    }

    public static mineBlock(lastBlock: Block, data: any): Block {
        let hash: string
        let timestamp: number
        const lastHash = lastBlock.hash
        let { difficulty } = lastBlock
        let nonce = 0

        do {
            nonce++
            timestamp = Date.now()
            difficulty = Block.adjustDifficulty(lastBlock, timestamp)
            hash = Block.hash(timestamp, lastHash, data, nonce, difficulty)
        } while (hash.substring(0, difficulty) !== '0'.repeat(difficulty))

        return new this(timestamp, lastHash, hash, data, nonce, difficulty)
    }

    public static hash(
        timestamp: number,
        lastHash: string,
        data: any,
        nonce: number,
        difficulty: number
    ): string {
        return ChainUtil.hash(
            `${timestamp}${lastHash}${data}${nonce}${difficulty}`
        ).toString()
    }

    public static blockHash(block: Block) {
        const { timestamp, lastHash, data, nonce, difficulty } = block
        return Block.hash(timestamp, lastHash, data, nonce, difficulty)
    }

    public static adjustDifficulty(lastBlock: Block, currentTime): number {
        let { difficulty } = lastBlock
        difficulty =
            lastBlock.timestamp + MINE_RATE > currentTime
                ? difficulty + 1
                : difficulty - 1
        return difficulty
    }
}
