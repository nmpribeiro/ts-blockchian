import { ec as EC } from 'elliptic'
import uuid = require('uuid')
import { SHA256 } from 'crypto-js'
import { ISignature } from '../cryptocurrency/wallet/Wallet'

const ec = new EC('secp256k1')

export class ChainUtil {
    public static genKeyPair(): EC.KeyPair {
        return ec.genKeyPair()
    }

    public static id(): string {
        return uuid.v1()
    }

    public static hash(data): string {
        return SHA256(JSON.stringify(data)).toString()
    }

    public static verifySignature(
        publicKey: string,
        signature: ISignature,
        dataHash: string
    ): boolean {
        return ec.keyFromPublic(publicKey, 'hex').verify(dataHash, signature)
    }
}
