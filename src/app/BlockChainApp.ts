import { App } from './App'
import { P2pServer } from './p2p.server'
import { BlockChain } from '../blockchain'
import { BlockChainController } from './controllers/BlockChainController'
import { AppController } from './controllers/AppController'
import { TransactionPool } from '../cryptocurrency/wallet/TransactionPool'
import { Wallet } from '../cryptocurrency/wallet/Wallet'
import { TransactionPoolController } from './controllers/TransactionPoolController'

export class BlockChainApp {
    private app: App
    private readonly p2pServer: P2pServer
    private readonly blockChain: BlockChain

    private readonly wallet: Wallet
    private readonly transactionPool: TransactionPool

    constructor() {
        this.blockChain = new BlockChain()
        this.app = new App()
        this.wallet = new Wallet()
        this.transactionPool = new TransactionPool()
        this.p2pServer = new P2pServer(this.blockChain, this.transactionPool)
    }

    public listen() {
        this.app.listen()
        this.p2pServer.listen()
    }

    public initializeAppControllers() {
        this.app.registerController('app', AppController)
        this.app.registerController('bc', BlockChainController)
        this.initializeBlockChainController()
        this.app.registerController('transactions', TransactionPoolController)
        this.initializeTransactionPoolController()
        this.app.initializeControllers()
    }

    public initializeBlockChainController() {
        const bcCtrl: BlockChainController = this.app.getController(
            'bc'
        ) as BlockChainController
        if (bcCtrl) {
            bcCtrl.setBlockchain(this.blockChain)
            bcCtrl.setSyncServer(this.p2pServer)
        }
    }

    private initializeTransactionPoolController() {
        const tpCtrl: TransactionPoolController = this.app.getController(
            'transactions'
        ) as TransactionPoolController

        if (tpCtrl) {
            tpCtrl.setTransactionPool(this.transactionPool)
            tpCtrl.setWallet(this.wallet)
            tpCtrl.setP2PSrv(this.p2pServer)
        }
    }
}
