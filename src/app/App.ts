import * as express from 'express'
import { Express } from 'express'
import bodyParser = require('body-parser')
import {
    BaseController,
    IBaseControllerClass,
} from './controllers/BaseController'
import { AppController } from './controllers/AppController'

export class App {
    private static HTTP_PORT: number = Number(process.env.HTTP_PORT) || 3001
    private express: Express = express()
    private controllers: Map<string, BaseController> = new Map()

    constructor() {
        // middleware
        this.express.use(bodyParser.json())
    }

    public registerController(
        controllerName: string,
        controllerClass: IBaseControllerClass
    ) {
        this.controllers.set(controllerName, new controllerClass(this.express))
    }

    public getController(controllerName: string): BaseController {
        return this.controllers.get(controllerName)
    }

    public initializeControllers() {
        this.controllers.forEach((controller, name) => {
            controller.init()
            console.log(`Controller ${name} was initialized`)
        })
    }

    public getServer(): Express {
        return this.express
    }

    public listen(): void {
        this.express.listen(App.HTTP_PORT, () => {
            console.log(
                '   WEB SERVER: Listening on address http://localhost:' +
                    App.HTTP_PORT
            )
        })
    }
}

const app = new App()
app.registerController('app', AppController)
if (require.main === module) {
    // true if file is executed
    app.initializeControllers()
    app.listen()
}
export default app
