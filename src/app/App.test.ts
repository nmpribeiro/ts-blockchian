import * as supertest from 'supertest'
import app from './App'

describe('app', () => {
    let request
    beforeEach(() => {
        app.initializeControllers()
        request = supertest(app.getServer())
    })

    it('should return a successful response for GET /', done => {
        request.get('/').expect(200, done)
    })
})
