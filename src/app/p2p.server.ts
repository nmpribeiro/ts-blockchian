import * as Websocket from 'ws'
import { Block, BlockChain } from '../blockchain'
import { ISyncServer } from './ISyncServer'
import { TransactionPool } from '../cryptocurrency/wallet/TransactionPool'
import { Transaction } from '../cryptocurrency/wallet/Transaction'

enum MESSAGE_TYPES {
    CHAIN = 'CHAIN',
    TRANSACTION = 'TRANSACTION',
}

interface IWSMessageChain {
    chain: Block[]
    transaction?: never
}
interface IWSMessageTransaction {
    chain?: never
    transaction: Transaction
}
interface IWSMessageType {
    type: string
}
type MessageType = IWSMessageType & (IWSMessageChain | IWSMessageTransaction)

export class P2pServer implements ISyncServer {
    private static P2P_PORT: number = Number(process.env.P2P_PORT) || 5001
    private PEERS: string[] = process.env.PEERS
        ? process.env.PEERS.split(',')
        : []

    private sockets: Websocket[] = []

    constructor(
        private blockchain: BlockChain,
        private transactionPool: TransactionPool
    ) {}

    public listen() {
        const server = new Websocket.Server({ port: P2pServer.P2P_PORT })
        server.on('connection', this.connectSocket)

        this.connectToPeers()

        console.log(
            `   P2P SERVER: Listening for peer-to-peer connections on ${P2pServer.P2P_PORT}`
        )
    }

    public syncChains() {
        this.sockets.forEach(socket => this.sendChain(socket))
    }

    public broadcastTransaction(transaction: Transaction) {
        this.sockets.forEach(socket =>
            this.sendTransaction(socket, transaction)
        )
    }

    private connectToPeers() {
        this.PEERS.forEach(peer => {
            const socket = new Websocket(peer)
            socket.on('open', () => this.connectSocket(socket))
        })
    }

    private connectSocket = (socket: Websocket) => {
        this.sockets.push(socket)
        console.log('Socket connected')
        this.messageHandler(socket)
        this.sendChain(socket)
    }

    private messageHandler(socket: Websocket) {
        socket.on('message', (message: string) => {
            const data: MessageType = JSON.parse(message)
            switch (data.type) {
                case MESSAGE_TYPES.CHAIN:
                    this.blockchain.replaceChain(data.chain)
                    break
                case MESSAGE_TYPES.TRANSACTION:
                    this.transactionPool.updateOrAddTransaction(
                        data.transaction
                    )
                    break
            }
        })
    }

    private sendChain(socket: Websocket) {
        this.webSocketSend(socket, {
            type: MESSAGE_TYPES.CHAIN,
            chain: this.blockchain.chain,
        })
    }

    private sendTransaction(socket: Websocket, transaction: Transaction) {
        this.webSocketSend(socket, {
            type: MESSAGE_TYPES.TRANSACTION,
            transaction,
        })
    }

    private webSocketSend(socket: Websocket, message: MessageType) {
        socket.send(JSON.stringify(message))
    }
}
