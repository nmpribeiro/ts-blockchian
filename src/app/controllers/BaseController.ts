import { Express, Request, Response } from "express";

export type IBaseControllerClass = new (app: Express) => BaseController;

export interface IBaseController {
    basePath: string;
}

export enum HttpMethod {
    DELETE,
    GET,
    HEAD,
    PATCH,
    POST,
    PUT,
}

export type IRequestHandler = (req: Request, res: Response) => Response | void;
export interface IRouteMap {
    path: string;
    method: HttpMethod;
    handler: IRequestHandler;
    middlewareArray?: any[];
}

export class BaseController implements IBaseController {
    public basePath: string;
    protected pathMap: IRouteMap[] = [];

    constructor(protected readonly app: Express) {}

    public init(): void {
        this.pathMap.forEach((value) => {
            const path = this.basePath + value.path;
            switch (value.method) {
                case HttpMethod.GET:
                    this.app.get(path, value.middlewareArray ? value.middlewareArray : [], value.handler);
                    break;
                case HttpMethod.POST:
                    this.app.post(path, value.middlewareArray ? value.middlewareArray : [], value.handler);
                    break;
                case HttpMethod.PATCH:
                    this.app.patch(path, value.middlewareArray ? value.middlewareArray : [], value.handler);
                    break;
                case HttpMethod.PUT:
                    this.app.put(path, value.middlewareArray ? value.middlewareArray : [], value.handler);
                    break;
                case HttpMethod.DELETE:
                    this.app.delete(path, value.middlewareArray ? value.middlewareArray : [], value.handler);
                    break;
                default:
                    console.warn(`Method ${value.method} not implemented`);
            }
        });
    }

    protected createHandler(path: string, method: HttpMethod, handler: IRequestHandler, middlewareArray: any[] = []): void {
        this.pathMap.push({ path, method, handler, middlewareArray });
    }
}
