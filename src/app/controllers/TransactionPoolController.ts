import { Express, Request, Response } from 'express'
import { BaseController, HttpMethod } from './BaseController'
import { TransactionPool } from '../../cryptocurrency/wallet/TransactionPool'
import { Wallet } from '../../cryptocurrency/wallet/Wallet'
import { P2pServer } from '../p2p.server'

const pathTransact = '/transact'
const pathTransactions = '/transactions'
const pathPublicKey = '/public-key'

export class TransactionPoolController extends BaseController {
    public basePath: string = '/'
    private transactionPool: TransactionPool
    private wallet: Wallet
    private p2pSrv: P2pServer

    constructor(app: Express) {
        super(app)

        // setup routes
        this.createHandler(pathTransactions, HttpMethod.GET, this.transactions)
        this.createHandler(pathTransact, HttpMethod.POST, this.transact)
        this.createHandler(pathPublicKey, HttpMethod.GET, this.publicKey)
    }

    public setWallet(wallet: Wallet): void {
        this.wallet = wallet
    }
    public setTransactionPool(transactionPool: TransactionPool): void {
        this.transactionPool = transactionPool
    }

    public setP2PSrv(p2pSrv: P2pServer): void {
        this.p2pSrv = p2pSrv
    }

    /**
     * Endpoint: GET transactions
     */
    private transactions = (req: Request, res: Response) =>
        res.json(this.transactionPool.getTransactions())

    private transact = (req: Request, res: Response) => {
        const {
            recipient,
            amount,
        }: { recipient: string; amount: number } = req.body
        const transaction = this.wallet.createTransaction(
            recipient,
            amount,
            this.transactionPool
        )
        this.p2pSrv.broadcastTransaction(transaction)
        res.redirect(pathTransactions)
    }

    private publicKey = (req: Request, res: Response) =>
        res.json({ publicKey: this.wallet.publicKey })
}
