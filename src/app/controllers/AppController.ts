import { Express, Request, Response } from 'express'
import { BaseController, HttpMethod } from './BaseController'

export class AppController extends BaseController {
    public basePath: string = '/'

    constructor(app: Express) {
        super(app)

        // setup routes
        this.createHandler('/', HttpMethod.GET, this.home)
    }

    /**
     * Endpoint: GET "/"
     */
    private home = (req: Request, res: Response) => res.json('Welcome to APP')
}
