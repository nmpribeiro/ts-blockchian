import { Express, Request, Response } from 'express'
import { BlockChain } from '../../blockchain'
import { BaseController, HttpMethod } from './BaseController'
import { ISyncServer } from '../ISyncServer'

const blocksPath = '/blocks'

export class BlockChainController extends BaseController {
    public basePath: string = '/'
    private bc: BlockChain
    private syncServer: ISyncServer

    constructor(app: Express) {
        super(app)
        this.bc = new BlockChain()

        // setup routes
        this.createHandler(blocksPath, HttpMethod.GET, this.blocks)
        this.createHandler('/mine', HttpMethod.POST, this.mine)
    }

    public setBlockchain(bc: BlockChain): void {
        this.bc = bc
    }

    public setSyncServer(syncServer: ISyncServer) {
        this.syncServer = syncServer
    }

    /**
     * Endpoint: GET blocksPath
     */
    private blocks = (req: Request, res: Response) => res.json(this.bc.chain)

    /**
     * Endpoint: POST "/mine"
     */
    private mine = (req: Request, res: Response) => {
        const block = this.bc.addBlock(req.body.data)
        console.log(`New block added: ${block.toString()}`)
        // todo: this sync could be wrong?
        this.syncServer.syncChains()
        res.redirect(blocksPath)
    }
}
